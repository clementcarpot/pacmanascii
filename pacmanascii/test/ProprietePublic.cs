namespace pacmanascii
{
    class ProprietePublic
    {
        public int Champspublic {get;set;}
    }

    class TestProprietePublic : AbstractTest
    {
        public TestProprietePublic(string name) : base(name){}
        override public bool test(){
            ProprietePublic pp = new ProprietePublic();
            pp.Champspublic = 42;
            if(pp.Champspublic == 42)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}