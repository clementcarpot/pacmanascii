namespace pacmanascii
{
    interface ITest
    {

        /*virtual protected void write OK
        Cette fonction ´ecrit la chaine ”[OK]:”
        dans la console avec la sous chaıne OK en vert.
        */
        virtual protected void writeOK() 
        {

        }
        /*
         virtual protected void write KO
        Cette fonction´ ecrit la chaine ”[KO]:”
        dans la console avec la sous chaˆıne KO en rouge.
        */
        virtual protected void writeKO()
        {

        }

        /*abstract public bool test
        Cette fonction ex´ecute le test.
        */
        abstract public bool test();

        /*virtual public void Main(string[]args)
        Cette fonction ex´ecute le test et affiche un resultat.
        Elle prends en argument un tableau de chaˆıne de caract`eres.
        */
        virtual public void Main(string[]args){}
    }

    class TestITest : ITest
    {
        public bool test()
        {
            return false;
        }
    }



}